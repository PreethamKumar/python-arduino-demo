import serial

# Create a serial object to communicate with Arduino.
COM_PORT = ""       # Replace with your COM PORT.
BAUD_RATE = 9600

serial_obj = serial.Serial(COM_PORT, BAUD_RATE)

while True:
    data = serial_obj.readline()
    data = data.decode("utf-8")
    print("Obtained from Arduino: " + data)
